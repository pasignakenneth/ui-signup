﻿using System;
using TheDragonIsHungry;
using TheDragonIsHungry.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EntryRendered), typeof(EntryRenderedRender))]
namespace TheDragonIsHungry.iOS
{
    public class EntryRenderedRender : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UIKit.UITextBorderStyle.None;

        }
    }
}
