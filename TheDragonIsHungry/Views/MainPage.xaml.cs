﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace TheDragonIsHungry
{
    public partial class MainPage : ContentPage
    {
        public static readonly BindableProperty BackButtonProperty = BindableProperty.Create("BackButton", typeof(string), typeof(MainPage), null);
        public static readonly BindableProperty BackButtonCommandProperty = BindableProperty.Create("BackButton_Command", typeof(ICommand), typeof(MainPage), null);


        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BackButton = "backbb.png";


        }
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new LandingPage());
        }
        public string BackButton
        {
            get
            {
                return (string)GetValue(BackButtonProperty);
            }
            set
            {
                SetValue(BackButtonProperty, value);
            }
        }
        public ICommand BackButton_Command
        {
            get
            {
                return (ICommand)GetValue(BackButtonCommandProperty);
            }
            set
            {
                SetValue(BackButtonCommandProperty, value);
            }
        }
    }
}
