﻿using System;
using Android.Content;
using TheDragonIsHungry;
using TheDragonIsHungry.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ButtonRendered), typeof(ButtonRenderedRender))]
namespace TheDragonIsHungry.Droid
{
    public class ButtonRenderedRender : ButtonRenderer
    {
        public ButtonRenderedRender(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            Control.SetAllCaps(false);
        }
    }
}
