﻿using System;
using Android.Content;
using TheDragonIsHungry;
using TheDragonIsHungry.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryRendered), typeof(EntryRenderedRender))]
namespace TheDragonIsHungry.Droid
{
    public class EntryRenderedRender : EntryRenderer
    {
        public EntryRenderedRender(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
                Control.Background = null;
            if (Control != null)
                Control.Gravity = Android.Views.GravityFlags.Center;
        }
    }
}
